<?php

namespace KDA\Livewire\Confirm;

use Illuminate\Support\ServiceProvider as PackageServiceProvider;


class ServiceProvider extends PackageServiceProvider
{
    public function register()
    {
        Package::make($this)->register();
    }

    //called after the trait were booted
    public function boot()
    {
        Package::get()->boot();
      

    }
}
