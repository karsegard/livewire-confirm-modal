<?php

namespace KDA\Livewire\Confirm;

use KDA\Laravel\Grow\Facades\Grow as Facade;
use KDA\Laravel\Grow\Facades\Grow;
use KDA\Laravel\Grow\Grow as Library;
use KDA\Laravel\Grow\Models\Page;
use KDA\Laravel\Grow\Policies\PagePolicy;
use KDA\Laravel\Package\Package as KDAPackage;
use KDA\Laravel\Package\Concerns\HasDatabaseDumps;
use KDA\Laravel\Package\Concerns\HasHelpers;
use KDA\Laravel\Package\Concerns\HasPolicies;
use KDA\Laravel\Package\RawPath;

class Package extends KDAPackage
{
  
    protected static string $package_name = 'livewire-confirm-modal';

    protected function setUp(): void
    {
        
    }

    public function boot(){
        
        parent::boot();
    }
}
